package com.example.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
//BI
public class BoardInfo {
    private String boardCode;
    private String boardStatus;
    private String tradingSessionID;
    private String tradSesStatus;
    private String name;
    private String shortname;
    private Date tradingDate;
    private String time;
    private double totalTrade;
    private  int totalStock;
    private int numSymbolAdvances;
    private int numSymbolNochange;
    private int numSymbolDeclines;
    private int dateNo;
    private double totalNormalTradedQttyRd;
    private double totalNormalTradedValueRd;
    private double totalNormalTradedQttyOd;
    private double totalNormalTradedValueOd;
    private double totalPTTradedQtty;
    private double totalPTTradedValue;
    private String marketCode;
}
