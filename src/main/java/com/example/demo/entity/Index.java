package com.example.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
//I
public class Index {
    private int idIndex;
    private String indexCode;
    private Double value;
    private String calTime;
    private Double change;
    private Double ratioChange;
    private Double totalQtty;
    private Double totalValue;
    private Date tradingDate;
    private String currentStatus;
    private Double totalStock;
    private Double priorIndexVal;
    private Double highestIndex;
    private Double lowestIndex;
    private Double closeIndex;
    private String typeIndex;
    private String indexName;
}
