package com.example.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
//TP
public class TopNPrice {
    private String symbol;
    private String boardCode;
    private int noTopPrice;
    private int numTopPrice;
    private double bestBidPrice;
    private int bestBidQtty;
    private double bestOfferPrice;
    private int bestOfferQtty;
}
