package com.example.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
//SI
public class StockInfo {
    private String symbol;
    private Double idSymbol;
    private String boardCode;
    private String tradingSessionID;
    private String tradSesStatus;
    private int securityTradingStatus;
    private int listingStatus;
    private String securityType;
    private Date issueDate;
    private String issuer;
    private String securityDesc;
    private int bestBidPrice;
    private int bestBidQtty;
    private int bestOfferQtty;
    private int bestOfferPrice;
    private double totalBidQtty;
    private double totalOfferQtty;
    private int basicPrice;
    private int floorPrice;
    private int ceilingPrice;
    private int floorPricePT;
    private int ceilingPricePT;
    private int parvalue;
    private int matchPrice;
    private int matchQtty;
    private int openPrice;
    private int priorOpenPrice;
    private int closePrice;
    private int priorClosePrice;
    private double totalVolumeTraded;
    private double totalValueTraded;
    private int midPx;
    private Date tradingDate;
    private String time;
    private int tradingUnit;
    private double totalListingQtty;
    private int dateNo;
    private int adjustQtty;
    private String referenceStatus;
    private int adjustRate;
    private int dividentRate;
    private int currentPrice;
    private int currentQtty;
    private int highestPrice;
    private int lowestPrice;
    private int priorPrice;
    private double matchValue;
    private int offerCount;
    private int bidCount;
    private double nm_TotalTradedQtty;
    private double nm_TotalTradedValue;
    private double pt_MatchQtty;
    private int pt_MatchPrice;
    private double pt_TotalTradedQtty;
    private double pt_TotalTradedValue;
    private double totalBuyTradingQtty;
    private int buyCount;
    private double totalBuyTradingValue;
    private double totalSellTradingQtty;
    private int sellCount;
    private double totalSellTradingValue;
    private double buyForeignQtty;
    private double buyForeignValue;
    private double sellForeignQtty;
    private double sellForeignValue;
    private int remainForeignQtty;
    private Date maturityDate;
    private double couponRate;
    private double totalBidQtty_OD;
    private double totalOfferQtty_OD;


    public void setIssueDate(String strIssueDate) {
        Date issueDate = null;
        try {
            issueDate = new SimpleDateFormat("yyyyMMdd-HH:mm:ss").parse(strIssueDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        this.issueDate = issueDate;
    }

    public void setMaturityDate(String str_maturityDate) {
        Date maturityDate = null;
        try {
            maturityDate = new SimpleDateFormat("yyyyMMdd-HH:mm:ss").parse(str_maturityDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        this.maturityDate = maturityDate;
    }
}
