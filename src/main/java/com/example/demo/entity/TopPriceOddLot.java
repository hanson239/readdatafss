package com.example.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
//PO
public class TopPriceOddLot {
    private String symbol;
    private String boardCode;
    private double bidPrice_1;
    private double bidQtty_1;
    private double bidPrice_2;
    private double bidQtty_2;
    private double bidPrice_3;
    private double bidQtty_3;
    private double offerPrice_1;
    private double offerQtty_1;
    private double offerPrice_2;
    private double offerQtty_2;
    private double offerPrice_3;
    private double offerQtty_3;
}
