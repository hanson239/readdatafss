package com.example.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
//EP
public class AutionMatch {
    private String actionType;
    private String symbol;
    private double matchPrice;
    private int qtty;
}
