package com.example.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
//S
public class Stock {
    private int idIndex;
    private String indexCode;
    private int idSymbol;
    private String symbol;
    private double totalQtty;
    private double weighted;
    private Date addDate;


}
