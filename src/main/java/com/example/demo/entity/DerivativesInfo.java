package com.example.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
//DI
public class DerivativesInfo {
    private String symbol;
    private double idSymbol;
    private String underlying;
    private String boardCode;
    private String tradingSessionID;
    private String tradSesStatus;
    private String securityTradingStatus;
    private String listingStatus;
    private String securityType;
    private double openInterest;
    private double openInterestChange;
    private Date firstTradingDate;
    private Date lastTradingDate;
    private double bestBidPrice;
    private double bestBidQtty;
    private double bestOfferPrice;
    private double bestOfferQtty;
    private double totalBidQtty;
    private double totalOfferQtty;
    private double basicPrice;
    private double floorPrice;
    private double ceilingPrice;
    private double floorPricePT;
    private double ceilingPricePT;
    private double matchPrice;
    private double matchQtty;
    private double openPrice;
    private double priorOpenPrice;
    private double openQtty;
    private double closePrice;
    private double priorClosePrice;
    private double closeQtty;
    private double totalVolumeTraded;
    private double totalValueTraded;
    private Date tradingDate;
    private String time;
    private int tradingUnit;
    private int dateNo;
    private double currentPrice;
    private int currentQtty;
    private double highestPrice;
    private double lowestPrice;
    private double matchValue;
    private int offerCount;
    private int bidCount;
    private double nm_TotalTradedQtty;
    private double nm_TotalTradedValue;
    private double pt_MatchQtty;
    private int pt_MatchPrice;
    private double pt_TotalTradedQtty;
    private double pt_TotalTradedValue;
    private double nm_BuyForeignQtty;
    private double pt_BuyForeignQtty;
    private double buyForeignQtty;
    private double buyForeignValue;
    private double nm_BuyForeignValue;
    private double pt_BuyForeignValue;
    private double sellForeignQtty;
    private double nm_SellForeignQtty;
    private double pt_SellForeignQtty;
    private double sellForeignValue;
    private double nm_SellForeignValue;
    private double pt_SellForeignValue;

    public void setFirstTradingDate(String str_firstTradingDate) {
        Date firstTradingDate = null;
        try {
            firstTradingDate = new SimpleDateFormat("dd/MM/yyyy").parse(str_firstTradingDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        this.firstTradingDate = firstTradingDate;
    }

    public void setLastTradingDate(String str_lastTradingDate) {
        Date lastTradingDate = null;
        try {
            lastTradingDate = new SimpleDateFormat("dd/MM/yyyy").parse(str_lastTradingDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.lastTradingDate = lastTradingDate;
    }
}
