package com.example.demo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

@SpringBootApplication
public class DemoApplication {

    public static void MapObjects(Map<String, String> objects){
        objects.put("EP","AutionMatch");
        objects.put("BI","BoardInfo");
        objects.put("DI","DerivativesInfo");
        objects.put("0","Hearbeat");
        objects.put("I","Index");
        objects.put("A","LogOn");
        objects.put("S","Stock");
        objects.put("SI","StockInfo");
        objects.put("TP","TopNPrice");
        objects.put("PO","TopPriceOddLot");
    }

    public static void MapFields(Map<String , String> fields){

        try {
            File fileDir = new File("fields.txt");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(fileDir), StandardCharsets.UTF_8));
            String str;
            String[] myArray;
            while ((str = bufferedReader.readLine()) != null) {
                //                System.out.println(str);
                myArray = str.split("\t");

                fields.put(myArray[0],myArray[1].trim());
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void handler(Map<String,String> fields, Map<String,String> objects,BufferedReader reader, int lineStart,BufferedWriter writer){
        Gson gson = new Gson();
        String line ="";
        String msgType;
        String valueField;
        String tagField;
        String[] message;
        Map<String,String> value = new HashMap<>();
        try{
            while ((line = reader.readLine()) != null) {
                message = line.split("\\0001");
                msgType = message[2].split("=")[1];
                if (msgType.equals("A")) continue;
                for (int i = 5; i < message.length; i++){
                    tagField = message[i].split("=")[0];
                    if (msgType.equals("PO"))
                        if (!tagField.equals("55") && !tagField.equals("425"))
                            tagField += "PO";
                    valueField = message[i].split("=")[1];
                    value.put(fields.get(tagField),valueField);
                }
                ObjectMapper mapper = new ObjectMapper();
                Class myClass = Class.forName("com.example.demo.entity." + objects.get(msgType));
                Object convertObj = mapper.convertValue(value,myClass);
                writer.write(gson.toJson(convertObj) + System.getProperty("line.separator"));
                value.clear();
            }
        } catch (Exception e) {
            System.out.println(line);
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {

        Map<String,String> objects = new HashMap<>();
        Map<String,String> fields = new HashMap<>();
        MapObjects(objects);
        MapFields(fields);

        try {
            File fileRead = new File("infogateclient.20200805.dat");
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileRead), StandardCharsets.UTF_8));

            File fileWrite = new File("result.dat");
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileWrite), StandardCharsets.UTF_8));
            writer.write("hello");






        } catch (Exception e) {
            System.out.println(line);
            e.printStackTrace();
        }
    }

}
